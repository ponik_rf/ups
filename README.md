# ups

Получение данных UPS через преобразователи интерфейсов Eth->RS232


## Установка

Клонирование репозитория:
```
git clone https://gitlab.com/ponik_rf/ups.git
```

Переходим в каталог проекта:
```
cd ./ups
```


Обновление composer:
```
composer update
```


## Использование 

```
php ./index.php <IP address> <IP port> <Device name> <Result type>
```

Где:

* **IP address** - IP адрес преобразователя (или UPS)
* **IP port** - IP port преобразователя (или UPS)
* **Device name** - Название поддерживаемого устройства (см. Список)
* **Result type** - Название формата результата (json или array (для отладки))

Список поддерживаемых устройств:

* **PowerComSKP** PowerCom серия SKP
    *  Smart Power Pro 1000 - S(2400) Пара провода Rx Tx должны быть перевернуты
* **Eaton9SX700I** Eaton9SX700I MGE SHUT протокол (можно попробовать другие подобные)
* **Eaton9SX700IV2** Eaton9SX700I Только немного измененный протокол (2020)
* **EatonPW9130I** На скорую руку, но работает и довольно быстро
* **SMX500RT1U** Работает, собирает основные данные.

Пример использования:

```
php ./index.php 172.16.103.237 4000 PowerComSKP array
```

## Получение результат

Скрипт печатает преобразовнный в `json` или `print_r` формат массив подобного содержания:

```
Array
(
    [code] => 0 
    [message] => Success
    [data] => Array
        (
            [INPUT_VOLTAGE] => 232.0
            [INPUT_FREQ] => 50.0
            [OUTPUT_VOLTAGE] => 232.0
            [OUTPUT_CURRENT] => 0%
            [OUTPUT_FREQ] => 50.0
            [BATTERY_VOLTAGE] => 27.3
            [BATTERY_LOW] => 
            [BATTERY_FAIL] => 
            [UPS_FAIL] => 
            [UPS_STATUS] => PROGRESS
            [UPS_TEMP] => 33.0
        )

)
```

Где:

* **code** - Номер ошибки (0 без ошибок)
* **message** - Сообщение ответа (success без ошибок)
* **data** - Содержит массив данных UPS


* **INPUT_VOLTAGE** - Поступающее напряжение
* **INPUT_FREQ** - Частота поступающего напряжения
* **OUTPUT_VOLTAGE** - Выходное напряжение 
* **OUTPUT_CURRENT** - Выходной ток (может быть указан в процентном соотношении)
* **OUTPUT_FREQ** - Частота выходного напряжения
* **BATTERY_VOLTAGE** - Напряжение батареи
* **BATTERY_LOW** - Напряжение на батарее аварийно мало
* **BATTERY_FAIL** - Проблема батареи
* **UPS_FAIL** - Проблема UPS
* **UPS_STATUS** - Статус UPS (PROGRESS - штатно, ONLINE - работа преобразователя)
* **UPS_TEMP** - Температура внутри UPS

## Пример получения результата для Eaton9SX700I:

```
Array
(
    [code] => 0
    [message] => Success
    [data] => Array
        (
            [INPUT_VOLTAGE] => 237.6
            [INPUT_FREQ] => 50
            [OUTPUT_VOLTAGE] => 230
            [OUTPUT_CURRENT] => 0
            [OUTPUT_FREQ] => 50
            [BATTERY_LOW] =>
            [BATTERY_FAIL] =>
            [UPS_FAIL] =>
            [UPS_STATUS] => PROGRESS
            [UPS_TEMP] => 30
            [LOAD_ACTIVE_WATT] => 75
            [LOAD_FULL_WATT] => 100
            [LOAD_CURRENT] => 0.5
            [LOAD_EFFICIENCY] => 65
            [BATTERY_CAPACITY] => 100
            [BATTERY_LIFE] => 55
        )

)
```

* **INPUT_VOLTAGE** - Поступающее напряжение
* **INPUT_FREQ** - Частота поступающего напряжения
* **OUTPUT_VOLTAGE** - Выходное напряжение 
* **OUTPUT_CURRENT** - Выходной ток (см.)
* **OUTPUT_FREQ** - Частота выходного напряжения
* **BATTERY_VOLTAGE** - Напряжение батареи
* **BATTERY_LOW** - Напряжение на батарее аварийно мало
* **BATTERY_FAIL** - Проблема батареи
* **UPS_FAIL** - Проблема UPS
* **UPS_STATUS** - Статус UPS (PROGRESS - штатно, ONLINE - работа преобразователя)
* **UPS_TEMP** - Температура внутри UPS
