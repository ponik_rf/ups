<?php

namespace Ponikrf\Ups\Devices;

use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteTo;

class SMX500RT1U extends Basic
{
    const MIN_VOLT = 11.0;
    const MAX_VOLT = 13.4;
    public function get()
    {
        unset($this->Properties['OUTPUT_VOLTAGE']);
        unset($this->Properties['OUTPUT_FREQ']);
        unset($this->Properties['BATTERY_FAIL']);
        unset($this->Properties['UPS_FAIL']);
        unset($this->Properties['UPS_FAIL']);
        
        $this->tempAndIFreq();
        $this->inputAndBatteryVoltage();
        $this->outputCurrent();
        $this->status();
        return $this->Properties;
    }

    public function command($cmd){
        $pkg = APCUPSSmart::createPackage($cmd);
        $this->Provider->request($pkg,2,0,2,function ($buffer){
            return APCUPSSmart::checkPackage($buffer);
        });
        return APCUPSSmart::splitPackage($this->Provider->getReadBuffer());
    }

    public function status(){
        $result = $this->command(":S");
        $ro = StringByte::getBytes($result['data'],1,3);
        $online = StringByteTo::uint8($ro);
        if ($online === 0) $this->Properties['UPS_STATUS'] = self::UPS_STATUS_ONLINE;
        else $this->Properties['UPS_STATUS'] = self::UPS_STATUS_PROGRESS;
    }

    public function tempAndIFreq(){
        $result = $this->command(":T");
        $bts = StringByte::getBytes($result['data'],2,2);
        $this->Properties['INPUT_FREQ'] = StringByteTo::uint16_be($bts)/10;
        $bts = StringByte::getBytes($result['data'],1,0);
        $this->Properties['UPS_TEMP'] = StringByteTo::uint8($bts);
    }

    public function inputAndBatteryVoltage(){
        $result = $this->command(":D");
        $bts = StringByte::getBytes($result['data'],2,0);
        $this->Properties['INPUT_VOLTAGE'] = StringByteTo::uint16_be($bts);
        $bts = StringByte::getBytes($result['data'],2,2);
        $this->Properties['BATTERY_VOLTAGE'] = StringByteTo::uint16_be($bts)/10;
        $procent = (int)(100*sqrt(($this->Properties['BATTERY_VOLTAGE'] - self::MIN_VOLT) / (self::MAX_VOLT - self::MIN_VOLT)));
        if ($procent > 100) $procent = 100;
        if ($procent < 0) $procent = 0;
        if ($procent < 20) 
            $this->Properties['BATTERY_LOW'] = true;
        else 
            $this->Properties['BATTERY_LOW'] = false;
        $this->Properties['BATTERY_PROCENT'] = $procent;
    }

    public function outputCurrent(){
        $result = $this->command(":L");
        $bts = StringByte::getBytes($result['data'],1,0);
        $this->Properties['OUTPUT_CURRENT'] = StringByteTo::uint8($bts);
    }


    public function inputSeen(){
        $result = $this->command(":M");
        $bts = StringByte::getBytes($result['data'],2,0);
        $this->Properties['INPUT_VOLTAGE_MIN'] = StringByteTo::uint16_be($bts);
        $bts = StringByte::getBytes($result['data'],2,2);
        $this->Properties['INPUT_VOLTAGE_MIN'] = StringByteTo::uint16_be($bts);
    }
}