<?php

namespace Ponikrf\Ups\Devices;

use Ponikrf\Indulib\Classes\ArrayByteTo;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteTo;


/* % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
 *
 * Protocol 1001
 *
 * OMNIVS Commands: (capital letters are literals, lower-case are variables)
 * :B     -> Bxxxxyy (xxxx/55.0: Hz in, yy/16: battery voltage)
 * :F     -> F1143_A (where _ = \0) Firmware version?
 * :L     -> LvvvvXX (vvvv/2.0: VAC out)
 * :P     -> P01000X (1000VA unit)
 * :S     -> Sbb_XXX (bb = 10: on-line, 11: on battery)
 * :V     -> V102XXX (firmware/protocol version?)
 * :Wt    -> Wt      (watchdog; t = time in seconds (binary, not hex), 
 *                   0 = disable; if UPS is not pinged in this interval, it
 *                   will power off the load, and then power it back on after
 *                   a delay.)
 *
 * % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
 *
 * The outgoing commands are sent with HID Set_Report commands over EP0
 * (control message), and incoming commands are received on EP1IN (interrupt
 * endpoint). The UPS completely ignores the conventions of Set_Idle (where
 * you NAK the interrupt read if you have no new data), so you constantly have
 * to poll EP1IN.
 *
 * The descriptors say that bInterval is 10 ms. You generally need to wait at
 * least 80-90 ms to get some characters back from the device.  If it takes
 * more than 250 ms, you probably need to resend the command.
 * 
 * All outgoing commands are followed by a checksum, which is 255 - (sum of
 * characters after ':'), and then by '\r'. All responses should start with
 * the command letter that was sent (no colon), and should be followed by
 * '\r'. If the command is not supported (or apparently if there is a serial
 * timeout internally), the previous response will be echoed back.
 *
 * % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
 *
 * SMARTPRO commands (3003):
 *
 * :A     -> ?          (start self-test)
 * :D     -> D7187      (? - doesn't match tripplite.c)
 * :F     -> F1019 A    firmware rev
 * :H__   -> H          (delay before action?)
 * :I_    -> I          (set flags for conditions that cause a reset?)
 * :J__   -> J          (set 16-bit unit ID)
 * :K#0   ->            (turn outlet off: # in 0..2; 0 is main relay)
 * :K#1   ->            (turn outlet on: # in 0..2)
 * :L     -> L290D_X
 * :M     -> M007F      (min/max voltage seen)
 * :N__   -> N
 * :P     -> P01500X    (max power)
 * :Q     ->            (while online: reboot)
 * :R     -> R<01><FF>  (query flags for conditions that cause a reset?)
 * :S     -> S100_Z0    (status?)
 * :T     -> T7D2581    (temperature, frequency)
 * :U     -> U<FF><FF>  (unit ID, 1-65535)
 * :V     -> V1062XX	(outlets in groups of 2-2-4, with the groups of 2
 * 			 individually switchable.)
 * :W_    -> W_		(watchdog)
 * :Z     -> Z		(reset for max/min; takes a moment to complete)
 * 
 * % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
 *
 * The SMARTPRO unit seems to be slightly saner with regard to message
 * polling. It specifies an interrupt in interval of 100 ms, but I just
 * started at a 2 second timeout to obtain the above table. 
 *
 * % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
 *
 * Commands from serial tripplite.c:
 *
 * :N%02X -- delay the UPS for provided time (hex seconds)
 * :H%06X -- reboot the UPS.  UPS will restart after provided time (hex s)
 * :A     -- begins a self-test
 * :C     -- fetches result of a self-test
 * :K1    -- turns on power receptacles
 * :K0    -- turns off power receptacles
 * :G     -- unconfirmed: shuts down UPS until power returns
 * :Q1    -- enable "Remote Reboot"
 * :Q0    -- disable "Remote Reboot"
 * :W     -- returns 'W' data
 * :L     -- returns 'L' data
 * :V     -- returns 'V' data (firmware revision)
 * :X     -- returns 'X' data (firmware revision)
 * :D     -- returns general status data
 * :B     -- returns battery voltage (hexadecimal decivolts)
 * :I     -- returns minimum input voltage (hexadecimal hertz) [sic]
 * :M     -- returns maximum input voltage (hexadecimal hertz) [sic]
 * :P     -- returns power rating
 * :Z     -- unknown
 * :U     -- unknown
 * :O     -- unknown
 * :E     -- unknown
 * :Y     -- returns mains frequency  (':D' is preferred)
 * :T     -- returns ups temperature  (':D' is preferred)
 * :R     -- returns input voltage    (':D' is preferred)
 * :F     -- returns load percentage  (':D' is preferred)
 * :S     -- enables remote reboot/remote power on
 */
class APCUPSSmart extends Basic
{
    /**
     * Create package 
     * 
     * +-------------+-------------+-----------+
     * |  Command    |   Reverse   |    END    | 
     * +-------------+-------------+-----------+
     * |   2 byte    |   1 byte    |   1 byte  |
     * +-------------+-------------+-----------+
     *
     * @param  string $cmd
     * @return string
     */
    public static function createPackage(string $cmd)
    {
        $Arr = StringByteTo::array($cmd);
        $Arr[] = ~(end($Arr)) & 0xFF;
        $Arr[] = 0x0D;
        return ArrayByteTo::string($Arr);
    }

    /**
     * Check return package
     *
     * @param $buffer
     * @return bool
     */
    public static function checkPackage($buffer){
        $arr = StringByteTo::array($buffer);
        if (end($arr) === 0x0D) return true;
        return false;
    }


    /**
     * Split package
     * 
     * +-------------+-------------+-----------+
     * |     Cmd     |    data     |    END    | 
     * +-------------+-------------+-----------+
     * |   1 byte    |  variable   |   1 byte  |
     * +-------------+-------------+-----------+
     *
    */
    public static function splitPackage($buffer){
        return [
            'cmd' => StringByte::getBytes($buffer,1,0),
            'data' => StringByte::getBytes($buffer,strlen($buffer)-2,1),
            'end' => StringByte::getBytes($buffer,1,strlen($buffer)+1),
        ];
    }
}