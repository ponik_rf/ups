<?php

namespace Ponikrf\Ups\Devices;

use Ponikrf\Indulib\Classes\ArrayByteFrom;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteTo;

class MGEShut extends Basic
{
    const LENGTHS = [
        1 => 0x11,
        2 => 0x22,
        3 => 0x33,
        4 => 0x44,
        5 => 0x55,
        6 => 0x66,
        7 => 0x77,
        8 => 0x88,
    ];
    const FROM_LENGTHS= [
        0x11 => 0x01,
        0x22 => 0x02,
        0x33 => 0x03,
        0x44 => 0x04,
        0x55 => 0x05,
        0x66 => 0x06,
        0x77 => 0x07,
        0x88 => 0x08,
    ];
    const MAIN_AC_STATUS_BIT = 0x01;
    const CHARGING_BIT = 0x02;
    const DISCHARGING_BIT = 0x04;
    const CAPACITY_LIMIT_BIT = 0x08;
    const BATTERY_FAIL_BIT = 0x010;
    const OUTPUT_STATUS_BIT = 0x20;
    const SHUTDOWN_PROGRESS_BIT = 0x40;
    const POWER_OVERLOAD_BIT = 0x80;

    public static function checksum($arrayBuffer)
    {
        $chk = 0;
        foreach ($arrayBuffer AS $value) $chk ^= $value;
        return $chk;
    }

    /**
     * +-------------+-------------+-------------------+------------+
     * | Packet type | Data length | Data bytes D0..Dn | Checksum-8 |
     * +-------------+-------------+-------------------+------------+
     * |   1 byte    |   1 byte    |     1-8 bytes     |   1 byte   |
     * +-------------+-------------+-------------------+------------+
     *
     * Packet type:
     * 0x01 (^A) – REQUEST (from host to UPS)
     * 0x04 (^D) – RESPONSE (from UPS to host)
     * 0x05 (^E) – NOTIFY (unsolicited transmission from UPS to host)
     * Add 0x80 to the above types to indicate the end of transmission
     *
     * @param int $pkgType
     * @param array $data
     * @return array
     */
    public static function createShutPackage(int $pkgType, array $data)
    {
        $start = [0x06];
        $pkgTypeArray = [$pkgType];
        $pkgLengthArray = [self::LENGTHS[count($data)]];
        $pkgCheckSumArray = MGEShut::checksum($data);
        return array_merge(
            $start,
            $pkgTypeArray,
            $pkgLengthArray,
            $data,
            [$pkgCheckSumArray]
        );
    }


    /**
     * GET REPORT command:
     *
     * +-------------+-------------+-------------+-------------+-----+-----+-----+-----+
     * | Rqst Type 1 | Rqst Type 2 |  Report ID  | Report Type | LSB | MSB | LSB | MSB |
     * +-------------+-------------+-------------+-------------+-----+-----+-----+-----+
     * |   1 byte    |   1 byte    |   1 byte    |   1 byte    |qwyuk |
     * +-------------+-------------+-------------+-------------+-----------------------+
     *
     * @param int $rTypeINT16
     * @param int $reportID
     * @param int $reportType
     * @param int $start
     * @param int $length
     * @return array
     */
    public static function getReportCommand(int $rTypeINT16,int $reportID, int $reportType, int $start, int $length)
    {
        $requestTypeArray = ArrayByteFrom::uint16_le($rTypeINT16);
        $reportIDArray = [$reportID];
        $reportTypeArray = [$reportType];
        $startArray = ArrayByteFrom::uint16_le($start);
        $lengthArray = ArrayByteFrom::uint16_le($length);

        return array_merge(
            $requestTypeArray,
            $reportIDArray,
            $reportTypeArray,
            $startArray,
            $lengthArray
        );
    }


    /**
     * проверка пакета
     *
     * @param $buffer
     * @return bool
     */
    public static function checkPackage($buffer){
        if (strlen($buffer) > 3)
        {
            $byte = StringByte::getBytes($buffer,1,2);
            $size = StringByteTo::uint8($byte);
            if (!array_key_exists($size,self::FROM_LENGTHS)) return false;
            if (strlen($buffer) == (self::FROM_LENGTHS[$size]+4)) return true;
        }
        return false;
    }

    public static function splitPackage($buffer){
        $byte = StringByte::getBytes($buffer,1,2);
        $size = StringByteTo::uint8($byte);
        $realSize = self::FROM_LENGTHS[$size];
        return [
            'code' => StringByte::getBytes($buffer,1,0),
            'data' => StringByte::getBytes($buffer,$realSize,3),
            'CRC' => StringByte::getBytes($buffer,1,$realSize+3),
        ];
    }


    public static function splitPostPackage($buffer){
        $byte = StringByte::getBytes($buffer,1,1);
        $size = StringByteTo::uint8($byte);
        $realSize = self::FROM_LENGTHS[$size];
        return [
            'code' => StringByte::getBytes($buffer,1,0),
            'data' => StringByte::getBytes($buffer,$realSize,2),
            'CRC' => StringByte::getBytes($buffer,1,$realSize+2),
        ];
    }
}