<?php

namespace Ponikrf\Ups\Devices;

use Ponikrf\Indulib\Classes\ArrayByte;
use Ponikrf\Indulib\Classes\ArrayByteTo;
use Ponikrf\Indulib\Classes\Cast;
use Ponikrf\Indulib\Classes\Memory;
use Ponikrf\Indulib\Classes\Rule;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteFrom;
use Ponikrf\Indulib\Classes\StringByteTo;

class Eaton9SX700IV2 extends MGEShut
{

    public $analize;
    public function get()
    {
        $this->Provider->setDebug(false);
        $this->ping();
        $this->getStatus();
        $this->getInputVoltage();
        $this->getLoadInfo();
        $this->getBatteryInfo();
        $this->Provider->write(ArrayByteTo::string([0x06]));
        return $this->Properties;
    }

    /**
     * Report 1
     *
     * STATUSES
     * IN BF BT BL DC UP
     * 01:00:01:00:00:00
     */
    public function getStatus()
    {
        $pkg_array = $this->command(1);
        $dataArray = StringByte::toArray($pkg_array['data']);


        $this->Properties['UPS_STATUS'] = ($dataArray[5] == 1) ? self::UPS_STATUS_PROGRESS : self::UPS_STATUS_ONLINE;
        $this->Properties['UPS_FAIL'] = false;
        $this->Properties['UPS_TEMP'] = 30;

        $pkg_array = $this->command(2);
        $dataArray = StringByte::toArray($pkg_array['data']);

        $this->Properties['BATTERY_FAIL'] = ($dataArray[1] == 1);
        $this->Properties['BATTERY_LOW'] = ($dataArray[0] == 1);
    }

    /**
     * Input voltage get
     *
     * ui16le   ui16le
     * FREQ     VOLT
     * F4:01    DB:08
     * value / 10
     */
    public function getInputVoltage()
    {
        $pkg_array = $this->command(49);
        $bytes = StringByte::getBytes($pkg_array['data'], 2, 0);
        $this->Properties['INPUT_FREQ'] = StringByteTo::uint16_le($bytes) / 10;
        $bytes = StringByte::getBytes($pkg_array['data'], 2, 2);
        $this->Properties['INPUT_VOLTAGE'] = StringByteTo::uint16_le($bytes) / 10;;
    }

    public function getLoadInfo()
    {
        $pkg_array = $this->command(0x42, 0x0000, 0x0100);
        $postCommand1 = $this->postCommand();
        $bytes = StringByte::getBytes($pkg_array['data'], 2, 0);
        $this->Properties['LOAD_ACTIVE_WATT'] = StringByteTo::uint16_le($bytes);
        $bytes = StringByte::getBytes($pkg_array['data'], 2, 2);
        $this->Properties['LOAD_FULL_WATT'] = StringByteTo::uint16_le($bytes);
        $bytes = StringByte::getBytes($pkg_array['data'], 2, 4);
        $this->Properties['OUTPUT_CURRENT'] = StringByteTo::uint16_le($bytes) / 10;
        $bytes = StringByte::getBytes($pkg_array['data'], 1, 6);
        $this->Properties['LOAD_EFFICIENCY'] = StringByteTo::uint8($bytes);

        $bytes = StringByte::getBytes($postCommand1['data'], 2, 0);
        $this->Properties['OUTPUT_FREQ'] = StringByteTo::uint16_le($bytes) / 10;
        $bytes = StringByte::getBytes($postCommand1['data'], 2, 3);
        $this->Properties['OUTPUT_VOLTAGE'] = StringByteTo::uint16_le($bytes) / 10;
    }


    /**
     * Battery info
     *
     * ui16le   ui16le
     * FREQ     VOLT
     * F4:01    DB:08
     * value / 10
     */
    public function getBatteryInfo()
    {
        $pkg_array = $this->command(6);
        $bytes = StringByte::getBytes($pkg_array['data'], 1, 0);
        $this->Properties['BATTERY_CAPACITY'] = StringByteTo::uint8($bytes);
        $bytes = StringByte::getBytes($pkg_array['data'], 2, 1);
        $this->Properties['BATTERY_LIFE'] = round(StringByteTo::uint16_le($bytes) / 60);
    }

    public function ping()
    {
        $ping = false;
        for ($i = 0; $i < 3; $i++) {
            if ($this->Provider->request(ArrayByteTo::string([0x16]), 2, 0, 1, function ($buffer) {
                return strlen($buffer);
            })) {
                $ping = true;
                break;
            }
        }
        if (!$ping) throw new \Exception("Устройство не отвечает");
    }


    public function command($reportID, $offset = 0x0000, $length = 0x0008)
    {
        $getReport = MGEShut::getReportCommand(
            //0x01A1,
            0x01A1,
            $reportID,
            0x03,
            $offset,
            $length
        );

        $shut = MGEShut::createShutPackage(0x81, $getReport);

        $result = $this->Provider->request(ArrayByteTo::string($shut), 2, 0, 1, function ($buffer) {
            return MGEShut::checkPackage($buffer);
        });

        if ($this->Provider->getReadBuffer() == StringByteFrom::array([0x06])) {
            return false;
        }

        if (!$result) throw new \Exception("Устройство не отвечает");
        $pkg_array = MGEShut::splitPackage($this->Provider->getReadBuffer());
        $pkg_array['data'] = substr($pkg_array['data'], 1);
        return $pkg_array;
    }

    public function postCommand()
    {
        $result = $this->Provider->request(ArrayByteTo::string([0x06]), 2, 0, 1, function ($buffer) {
            return true;
        });

        if (!$result) throw new \Exception("Устройство не отвечает");
        $pkg_array = MGEShut::splitPostPackage($this->Provider->getReadBuffer());
        return $pkg_array;
    }


    public function getWakeUp()
    {
        if ($this->command(41)) {
            $pkg_array = MGEShut::splitPackage($this->Provider->getReadBuffer());
            $bytes = StringByte::getBytes($pkg_array['data'], 4, 1);
            echo "WakeUP sec - " . (StringByteTo::uint32_le($bytes));
            $bytes = StringByte::getBytes($pkg_array['data'], 2, 1);
            echo " | small WakeUP sec - " . (StringByteTo::uint16_le($bytes));
            echo PHP_EOL;
        }
    }
    /*
    Scan functions
    public function first(){
        for($i = 1;$i<50;$i++){
            if ($this->command($i)) {
                $pkg_array = MGEShut::splitPackage($this->Provider->getReadBuffer());
                $pkg_array['data'] = substr($pkg_array['data'],1);
                $this->analize[$i] = $pkg_array['data'];
            }
        }
    }

    public function diff(){
        foreach ($this->analize AS $i => $value){
            if ($this->command($i)) {
                $pkg_array = MGEShut::splitPackage($this->Provider->getReadBuffer());
                $pkg_array['data'] = substr($pkg_array['data'],1);
                //echo " I = $i".PHP_EOL;
                //StringByte::dump($pkg_array['data']);
                if ($pkg_array['data'] != $value) {
                    echo $i . " - ";
                    echo trim(StringByte::printBytes($value, true));
                    echo "->";
                    echo StringByte::printBytes($pkg_array['data'], true);
                    $this->analize[$i] = $pkg_array['data'];
                }
            }
        }
    }
    */
}
