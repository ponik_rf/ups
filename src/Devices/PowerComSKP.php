<?php

namespace Ponikrf\Ups\Devices;

class PowerComSKP extends Basic
{
    public function get()
    {
        $this->Provider->request("Q1\r",2,0,1,function ($buffer){
            return (substr($buffer,-1) == "\r");
        });

        $result = trim($this->Provider->getReadBuffer());
        $ResultArray = explode(' ',$result);
        if (count($ResultArray) != 8) throw new \Exception("Не удалось получить данные");

        $ResultArray[0]= str_replace('(','',$ResultArray[0]);
        $statusBits = $ResultArray[7];
        $this->Properties['INPUT_VOLTAGE'] = $ResultArray[0];
        $this->Properties['INPUT_FREQ'] = $ResultArray[4];

        $this->Properties['OUTPUT_VOLTAGE'] = $ResultArray[2];
        $this->Properties['OUTPUT_FREQ'] = $ResultArray[4];
        $this->Properties['OUTPUT_CURRENT'] = ((int)$ResultArray[3]);

        $this->Properties['BATTERY_VOLTAGE'] = $ResultArray[5];
        $this->Properties['BATTERY_FAIL'] = false;
        $this->Properties['BATTERY_LOW'] = ($statusBits[1] == "1");

        $this->Properties['UPS_FAIL'] = ($statusBits[3] == "1");
        $this->Properties['UPS_STATUS'] = ($statusBits[4] == "1")?self::UPS_STATUS_PROGRESS:self::UPS_STATUS_ONLINE;
        $this->Properties['UPS_TEMP'] = $ResultArray[6];

        return $this->Properties;
    }
}