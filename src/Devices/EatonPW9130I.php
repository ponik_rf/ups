<?php

namespace Ponikrf\Ups\Devices;

use Ponikrf\Indulib\Classes\ArrayByte;
use Ponikrf\Indulib\Classes\ArrayByteTo;
use Ponikrf\Indulib\Classes\Cast;
use Ponikrf\Indulib\Classes\Memory;
use Ponikrf\Indulib\Classes\Rule;
use Ponikrf\Indulib\Classes\StringByte;
use Ponikrf\Indulib\Classes\StringByteFrom;
use Ponikrf\Indulib\Classes\StringByteTo;

class EatonPW9130I extends Basic
{

    public $analize;
    public function get()
    {
        $this->Provider->setDebug(false);
        $this->ping();
        $this->getStatus();
        $this->getLoadInfo();
        return $this->Properties;
    }

    public function getStatus()
    {
        $result = $this->Provider->request(ArrayByteTo::string([0xab, 0x01, 0x3b, 0x19]), 2, 0, 1, function ($buffer) {
            $bytes = StringByte::getBytes($buffer, 1, 2);
            $res = StringByteTo::uint8($bytes);
            if (strlen($buffer) === $res + 5) return true;
            return false;
        });

        if (!$result) throw new \Exception("Устройство не отвечает");

        $buffer = $this->Provider->getReadBuffer();

        $bytes = StringByte::getBytes($buffer, 1, 0x19);
        $status = StringByteTo::uint8($bytes);

        $this->Properties['UPS_FAIL'] = 0;

        switch ($status) {
            case 0x01:
                $this->Properties['UPS_STATUS'] = self::UPS_STATUS_PROGRESS;
                $this->Properties['BATTERY_FAIL'] = true;
                break;
            case 0x02:
                $this->Properties['UPS_STATUS'] = self::UPS_STATUS_ONLINE;
                $this->Properties['BATTERY_FAIL'] = false;
                break;
            case 0x03:
                $this->Properties['UPS_STATUS'] = self::UPS_STATUS_PROGRESS;
                $this->Properties['BATTERY_FAIL'] = false;
                break;
        }
    }

    public function getLoadInfo()
    {
        $result = $this->Provider->request(ArrayByteTo::string([0xab, 0x01, 0x34, 0x20]), 2, 0, 1, function ($buffer) {
            $bytes = StringByte::getBytes($buffer, 1, 2);
            $res = StringByteTo::uint8($bytes);
            if (strlen($buffer) === $res + 5) return true;
            return false;
        });

        if (!$result) throw new \Exception("Устройство не отвечает");
        $buffer = $this->Provider->getReadBuffer();

        $bytes = StringByte::getBytes($buffer, 4, 0x04);
        $this->Properties['LOAD_ACTIVE_WATT'] = StringByteTo::uint32_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x08);
        $this->Properties['LOAD_FULL_WATT'] = StringByteTo::uint32_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x0C);
        $this->Properties['OUTPUT_FREQ'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x10);
        $this->Properties['INPUT_FREQ'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x14);
        $this->Properties['INPUT_FREQ_BYPASS'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x18);
        $this->Properties['BATTERY_VOLTAGE'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x1C);
        $this->Properties['BATTERY_CAPACITY'] = StringByteTo::uint32_le($bytes);

        if ($this->Properties['BATTERY_CAPACITY'] <= 20)
            $this->Properties['BATTERY_LOW'] = true;
        else
            $this->Properties['BATTERY_LOW'] = false;

        $bytes = StringByte::getBytes($buffer, 4, 0x20);
        $this->Properties['BATTERY_LIFE'] = round(StringByteTo::uint32_le($bytes) / 60);

        $bytes = StringByte::getBytes($buffer, 4, 0x24);
        $this->Properties['LOAD'] = StringByteTo::uint32_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x28);
        $this->Properties['INPUT_VOLTAGE'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x2C);
        $this->Properties['INPUT_VOLTAGE_BYPASS'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x38);
        $this->Properties['OUTPUT_CURRENT'] = StringByteTo::float_le($bytes);

        $bytes = StringByte::getBytes($buffer, 4, 0x54);
        $this->Properties['OUTPUT_VOLTAGE'] = StringByteTo::float_le($bytes);
    }

    public function ping()
    {
        $ping = false;
        for ($i = 0; $i < 3; $i++) {
            if ($this->Provider->request(ArrayByteTo::string([0xab, 0x01, 0xa0, 0xb4]), 2, 0, 1, function ($buffer) {
                return strlen($buffer);
            })) {
                $ping = true;
                break;
            }
        }
        if (!$ping) throw new \Exception("Устройство не отвечает");
    }
}
