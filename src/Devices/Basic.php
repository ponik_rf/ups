<?php

namespace Ponikrf\Ups\Devices;

use Ponikrf\Indulib\Providers\ProviderInterface;

class Basic
{
    const UPS_STATUS_ONLINE = 'ONLINE';
    const UPS_STATUS_PROGRESS = 'PROGRESS';

    protected $Provider;

    protected $Properties = [
        /* VOLTAGE PROPERTIES */
        'INPUT_VOLTAGE' => 0.0,
        'INPUT_FREQ' => 0.0,
        'OUTPUT_VOLTAGE' => 0.0,
        'OUTPUT_CURRENT' => 0.0,
        'OUTPUT_FREQ' => 0.0,

        /* BATTERY PROPERTIES */
        'BATTERY_LOW' => false,
        'BATTERY_FAIL' => false,

        /* UPS PROPERTIES */

        'UPS_FAIL' => false,
        'UPS_STATUS' => 'ONLINE',
        'UPS_TEMP' => 0.0,
    ];

    public function __construct(ProviderInterface $provider)
    {
        $this->Provider = $provider;
    }

    public function get()
    {
        return $this->Properties;
    }

}