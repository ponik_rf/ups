<?php

namespace Ponikrf\Ups\Classes;

class Response
{

    protected static $responseType = 'json';
    public static function setResponseType($type){
        static::$responseType = strtolower($type);
    }
    public static function send(int $code = 0, string $message = '', array $data = [])
    {
        $array = ['code' => $code, 'message' => $message, 'data' => $data];
        switch (static::$responseType){
            case "json":
                $result = json_encode($array);
                break;
            default:
                $result = print_r($array,true);
                break;
        }
        die($result);
    }
}