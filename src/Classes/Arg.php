<?php

namespace Ponikrf\Ups\Classes;

class Arg
{
    protected $args = [];

    public function __construct(array $args)
    {
        $this->args = $args;
    }

    public function get($i,$default = ''){
        if (array_key_exists($i,$this->args)) return $this->args[$i];
        return $default;
    }

}