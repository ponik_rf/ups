<?php

include("./vendor/autoload.php");

use Ponikrf\Indulib\Classes\Console;
use Ponikrf\Ups\Classes\Arg;
use Ponikrf\Ups\Classes\Response;

$Args = new Arg($argv);

$INPUT_ARG['arg_ip'] = $Args->get(1, false);
$INPUT_ARG['arg_port'] = $Args->get(2, false);
$INPUT_ARG['arg_type'] = trim($Args->get(3, false));
$INPUT_ARG['arg_resp'] = trim($Args->get(4, 'json'));

Response::setResponseType($INPUT_ARG['arg_resp']);

foreach ($INPUT_ARG AS $key => $value)
    if (!$value)
        die(Console::print("Argument $key not found \n","red",false,false,true));

$ListUPS = include("./UPS.php");

if (!array_key_exists($INPUT_ARG['arg_type'], $ListUPS)){
    \Ponikrf\Indulib\Classes\Console::print("Devices type ".$INPUT_ARG['arg_type']." not found".PHP_EOL,"red");
    echo "Supported Devices type:".PHP_EOL;
    foreach ($ListUPS AS $ups=>$desc) echo "$ups".PHP_EOL;
    die();
}

$Provider = new \Ponikrf\Indulib\Providers\SocketProvider();
$Provider->setAddress($INPUT_ARG['arg_ip'] . ":" . $INPUT_ARG['arg_port']);
$Provider->setDebug(false);

try {
    $Provider->connect();
} catch (\Ponikrf\Indulib\Exceptions\ProviderException $e) {
    Response::send(-1, $e->getMessage());
}

$class = "\\Ponikrf\\Ups\\Devices\\".$INPUT_ARG['arg_type'];

/** @var $Device \Ponikrf\Ups\Devices\Basic */
$Device = new $class($Provider);
try{
    $Properties = $Device->get();
    Response::send(0,'Success',$Properties);
}catch (\Exception $exception){
    Response::send(-1,$exception->getMessage());
}

//include ("./Devices/".$INPUT_ARG['arg_type']);

